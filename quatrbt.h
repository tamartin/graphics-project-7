#ifndef QUATRBT_H
#define QUATRBT_H

#include <iostream>
#include <cassert>

#include "matrix4.h"
#include "quat.h"

class QuatRBT {
  Cvec3 t_; // translation component
  Quat r_;  // rotation component represented as a quaternion

public:
  QuatRBT() : t_(0) {
    assert(norm2(Quat(1,0,0,0) - r_) < CS150_EPS2);
  }

  QuatRBT(const Cvec3& t, const Quat& r) {
    t_ = t;
    r_ = r;
  }

  explicit QuatRBT(const Cvec3& t) {
    t_ = t;
    r_ = Quat();
  }

  explicit QuatRBT(const Quat& r) {
    t_ = Cvec3(0,0,0);
    r_ = r;
  }

  Cvec3 getTranslation() const {
    return t_;
  }

  Quat getRotation() const {
    return r_;
  }

  QuatRBT& setTranslation(const Cvec3& t) {
    t_ = t;
    return *this;
  }

  QuatRBT& setRotation(const Quat& r) {
    r_ = r;
    return *this;
  }

  Cvec4 operator * (const Cvec4& a) const {
    
    return r_ * a + Cvec4(t_[0], t_[1], t_[2], 0); // may need to change t_ to a Cvec4
  }

  QuatRBT operator * (const QuatRBT& a) const {
    Cvec3 t1 = a.getTranslation();
    Cvec4 t2 = r_ * Cvec4(t1[0], t1[1], t1[2], 0);
    return QuatRBT(Cvec3(t2[0], t2[1], t2[2]) + t_, r_ * a.getRotation());
  }
};

inline QuatRBT inv(const QuatRBT& tform) {
  Quat q = inv(tform.getRotation());
  Cvec3 t1 = tform.getTranslation();
  Cvec4 t2 = Cvec4(t1[0], t1[1], t1[2], 0);
  Cvec4 t3 = q * t2;
  return QuatRBT(Cvec3(t3[0], t3[1], t3[2]) * -1, inv(tform.getRotation()));
}

inline QuatRBT transFact(const QuatRBT& tform) {
  return QuatRBT(tform.getTranslation(), Quat(1,0,0,0));
}

inline QuatRBT linFact(const QuatRBT& tform) {
  return QuatRBT(Cvec3(0,0,0), tform.getRotation());
}

inline Matrix4 rigTFormToMatrix(const QuatRBT& tform) {
  Matrix4 m;
  m = quatToMatrix(tform.getRotation());
  Cvec3 t = tform.getTranslation();
  return Matrix4::makeTranslation(tform.getTranslation()) * m; // may need to change t_ to a Cvec4
}

#endif

